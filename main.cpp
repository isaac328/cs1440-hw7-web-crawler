#include <iostream>
#include <unordered_set>
#include <regex>
#include <sstream>

#include <curlpp/cURLpp.hpp>
#include <curlpp/Easy.hpp>
#include <curlpp/Options.hpp>


void crawl(std::string url, std::unordered_set<std::string>* visitedSites, int depth, int maxDepth, bool firtRun)
{
    //check if its in the set
    std::unordered_set<std::string>::const_iterator it = visitedSites->find(url);
    
    //if we've hit the max depth or its in the set then return
    if(depth >= maxDepth || it != visitedSites->end())
        return;
        
    //insert it in the set
    visitedSites->insert(url);
    
    //if this isnt the first iteration then print the site
    if(!firtRun)
    {
        for(int i = 0; i < depth; i++)
        {
            std::cout << "\t";
        }
        std::cout << url << std::endl;
    }
    
    //curl stuff
    curlpp::Cleanup cleaner;
    curlpp::Easy request;
    curlpp::options::Url curlUrl(url);
    
    request.setOpt(curlUrl);
    curlpp::options::FollowLocation follow(true);
    request.setOpt(follow);
    
    //get the html
    std::ostringstream os;
    os << curlpp::options::Url(url) << std::endl;
    std::string html = os.str();
    
    //regex, 0th group is full match, 1st is protocol, 2nd matches if its a relative link, 3rd group mathches if its a full link
    std::string reg = "<a href=\"(https?://)?(/[^ #]+)?([^ #]+)?\"";
    std::regex reg1(reg);
    
    std::smatch m;
    
    //std::vector<std::string> nextURLs;
    
    while (std::regex_search (html, m, reg1)) 
    {
        try
        {
            //if its a relative link
            if(m[2] != "")
            {
                std::string relativeURL = m[2];
                crawl(url + relativeURL, visitedSites, depth+1, maxDepth, false);
                
            }
            else
            {
                std::string protocol = m[1];
                std::string newURL = m[3];
                crawl(protocol + newURL, visitedSites, depth+1, maxDepth, false);
            }
    		html = m.suffix().str(); 
        }
        catch(curlpp::LibcurlRuntimeError error)
        {
            for(int i = 0; i < depth; i++)
                std::cout << "\t";
                
            std::cout << "Error on url: " << m[3] << std::endl;
        }
	}
}

void crawl(std::string url, int maxDepth)
{
    std::unordered_set<std::string>* visitedSites = new std::unordered_set<std::string>();
    crawl(url, visitedSites, -1, maxDepth, true);
    delete visitedSites;
}

int main(int argc, char** argv) {
    std::string there = "http://cs.usu.edu";
    //std::string there = "http://www.alexisaac.com";
    
    int maxDepth = 3;

    if (argc > 1)
        there = argv[1];

    if (argc > 2)
        maxDepth = atoi(argv[2]);

    std::cout << "Recursively crawling the web to a depth of " << maxDepth << " links beginning from " << there << std::endl;

    //  _____ ___  ___   ___  
    // |_   _/ _ \|   \ / _ (_)
    //   | || (_) | |) | (_) |
    //   |_| \___/|___/ \___(_)
    //                        
    // 0. Define a recursive function that will, given a URL and a maximum
    //    depth, follow all hyperlinks found until the maximum depth is
    //    reached. You may define a wrapper function to make calling this
    //    recursive function from main() easier.
    //
    // 1. Your recursive function will create a new curlpp::Easy object each
    //    time it's called.  it should also clean up after itself.
    //
    // 2. Your recursive function needs to account for relative URLs. For
    //    example, the address of Don Knuth's FAQ page is
    //    http://www-cs-faculty.stanford.edu/~knuth/faq.html. If you inspect
    //    the contents of the page with your browser's developer tools
    //    (Ctrl-Shift-I) or view the source (Ctrl-U), you'll see that many of
    //    the links therein do not begin with
    //    http://www-cs-faculty.stanford.edu/.
    //
    //    Your recursive function needs to account for this by remembering what
    //    the current domain name is, and being prepared to prepend that to the
    //    URL it parses out of any given hyperlink.
    //
    // 3. Your recursive function should skip hyperlinks beginning with # -
    //    they refer to locations on the same page.
    //
    // 4. Your recursive function must also keep track of all pages it's
    //    visited so that it doesn't waste time visiting the same one again and
    //    again. This is where the std::map comes in handy. Your recursive
    //    function takes it as an argument, and returns it, possibly modified,
    //    after each invocation.
    
    
    crawl(there, maxDepth);

    return 0;
}
